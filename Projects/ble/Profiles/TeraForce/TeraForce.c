/**************************************************************************************************
  Filename:       accelerometer.c
  Revised:        $Date: 2011-11-21 15:26:10 -0800 (Mon, 21 Nov 2011) $
  Revision:       $Revision: 28439 $

  Description:    Accelerometer Profile


  Copyright 2009 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE, 
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com. 
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "hal_led.h"
#include "TeraForce.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SB_SERVAPP_NUM_ATTR_SUPPORTED        22

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

// SMART BIA Service UUID
CONST uint8 teraForceServUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(TERAFORCE_SERVICE_UUID), HI_UINT16(TERAFORCE_SERVICE_UUID)
};

// SMART BIA Start Measurement UUID
CONST uint8 teraForceStartMeasUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(FORCE_START_MEAS_UUID), HI_UINT16(FORCE_START_MEAS_UUID)
};

// TERAFORCE FORCE  VALUE  UUID
CONST uint8 teraForce_ForceValUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(FORCE_VAL_UUID), HI_UINT16(FORCE_VAL_UUID)
};

//  SMART BIA Q VALUE  UUID
//CONST uint8 biaQvalUUID[ATT_BT_UUID_SIZE] =
//{ 
//  LO_UINT16(BIA_Q_VAL_UUID), HI_UINT16(BIA_Q_VAL_UUID)
//};

// SMART BIA MEASURE Point UUID
//CONST uint8 biaMeasPointUUID[ATT_BT_UUID_SIZE] =
//{ 
//  LO_UINT16(BIA_MEAS_POINT_UUID), HI_UINT16(BIA_MEAS_POINT_UUID)
//};

// SMART BIA CABLE CONNECTED  UUID
//CONST uint8 biaCableConnectedUUID[ATT_BT_UUID_SIZE] =
//{ 
//  LO_UINT16(BIA_CABLE_CONNECTED_UUID), HI_UINT16(BIA_CABLE_CONNECTED_UUID)
//};

// SMART BIA FREQUENCY  UUID
//CONST uint8 biaFrequencyUUID[ATT_BT_UUID_SIZE] =
//{ 
//  LO_UINT16(BIA_FREQUENCY_UUID), HI_UINT16(BIA_FREQUENCY_UUID)
//};
/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static teraForceCBs_t *teraForce_AppCBs = NULL;


/*********************************************************************
 * Profile Attributes - variables
 */

// Smart Bia Service attribute
static CONST gattAttrType_t teraForceService = { ATT_BT_UUID_SIZE, teraForceServUUID };


// Smart Bia Meas Start Characteristic Properties
static uint8 forceStartMeasCharProps = GATT_PROP_READ | GATT_PROP_WRITE;

// Smart Bia Meas Start Characteristic Value
static uint8 teraForceStartMeas = FALSE;

// Smart Bia Meas Start Characteristic user description
static uint8 teraForceStartMeasUserDesc[18] = "Force Start Meas\0";


// Froce value Characteristic Properties
static uint8 teraForce_Force_valCharProps = GATT_PROP_NOTIFY ;

// Force value 
static uint8 teraForce_Force_val[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// Force value Characteristic user description
static uint8 teraForceValCharUserDesc[11] = "Force Val\0";


// Force Val Characteristic Configs
static gattCharCfg_t teraForce_ForceValConfig[GATT_MAX_NUM_CONN];
// static gattCharCfg_t smartbiaQvalConfig[GATT_MAX_NUM_CONN];
// static gattCharCfg_t smartbiaCableConnectedConfig[GATT_MAX_NUM_CONN];
//static gattCharCfg_t accelZConfigCoordinates[GATT_MAX_NUM_CONN];


/*********************************************************************
 * Profile Attributes - Table
 */
static gattAttribute_t teraForceAttrTbl[] = 
{
  // SmartBia Service
  { 
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                   /* permissions */
    0,                                  /* handle */
    (uint8 *)&teraForceService                /* pValue */
  },
  
    // Force Start Meas. Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      (uint8*)&forceStartMeasCharProps 
    },

      //Force Start Meas. Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, teraForceStartMeasUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0,
        (uint8*)&teraForceStartMeas 
      },

      // TeraForce Start Meas. User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0,
        teraForceStartMeasUserDesc 
      },   
    // I value Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      (uint8*)&teraForce_Force_valCharProps 
    },
      // I value Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, teraForce_ForceValUUID },
        0, 
        0, 
        (uint8*)teraForce_Force_val
      },      
      // I value  Characteristic configuration
      { 
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        (uint8*)teraForce_ForceValConfig 
      },
      // I value  Characteristic User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        (uint8*)teraForceValCharUserDesc
      }/*,       

    // Q value Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      (uint8*)&smartbiaQvalCharProps 
    },
      // Q value Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, biaQvalUUID },
        0, 
        0, 
        (uint8*)smartbiaQval
      },      
      // Q value  Characteristic configuration
      { 
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        (uint8*)smartbiaQvalConfig 
      },
      // Q value  Characteristic User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        (uint8*)smartbiaQvalCharUserDesc
      },
      
      
      // Cable Connected Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      (uint8*)&smartbiaCableConnectedCharProps 
    },
    // Cable Connnected value Characteristic Value
    { 
      { ATT_BT_UUID_SIZE, biaCableConnectedUUID },
      0, 
      0, 
      (uint8*)&smartbiaCableConnected
    },
    // Cable Connnected value  Characteristic configuration
    { 
      { ATT_BT_UUID_SIZE, clientCharCfgUUID },
      GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
      0, 
      (uint8*)smartbiaCableConnectedConfig
    },
    // Cable Connnected Characteristic User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        (uint8*)smartbiaCableConnectedCharUserDesc
      },



   //  Meas. Point Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      (uint8*)&smartbiaMeasPointCharProps 
    },

      // Meas. Point Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, biaMeasPointUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0,
        (uint8*)&smartbiaMeasPoint 
      },

      // Meas. Point User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0,
        (uint8*)smartbiaMeasPointUserDesc 
      },     

    
      
      // SmartBia Frequency Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      (uint8*)&smartbiaFrequencyCharProps 
    },

      //SmartBia Frequency Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, biaFrequencyUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0,
        (uint8*)&smartbiaFrequency 
      },

      // SmartBia Frequency User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0,
        (uint8*)smartbiaFrequencyUserDesc
      }   
    */
};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static uint8 teraForce_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                               uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen );
static bStatus_t teraForce_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                    uint8 *pValue, uint8 len, uint16 offset );

static void teraForce_HandleConnStatusCB( uint16 connHandle, uint8 changeType );

/*********************************************************************
 * PROFILE CALLBACKS
 */
//  Accelerometer Service Callbacks
CONST gattServiceCBs_t  teraForceCBs =
{
  teraForce_ReadAttrCB,  // Read callback function pointer
  teraForce_WriteAttrCB, // Write callback function pointer
  NULL               // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn     TeraForce_AddService
 *
 * @brief   Initializes the TeraForce service by
 *          registering GATT attributes with the GATT server. Only
 *          call this function once.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t TeraForce_AddService( uint32 services )
{
  uint8 status = SUCCESS;

  // Initialize Client Characteristic Configuration attributes
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, teraForce_ForceValConfig );
  // Register with Link DB to receive link status change callback
  VOID linkDB_Register( teraForce_HandleConnStatusCB );  

  if ( services & TERAFORCE_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( teraForceAttrTbl, 
                                          GATT_NUM_ATTRS( teraForceAttrTbl ),
                                          &teraForceCBs );
  }

  return ( status );
}

/*********************************************************************
 * @fn      TeraForce_RegisterAppCBs
 *
 * @brief   Does the profile initialization.  Only call this function
 *          once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t TeraForce_RegisterAppCBs( teraForceCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
    teraForce_AppCBs = appCallbacks;
    
    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}


/*********************************************************************
 * @fn     TeraForce_SetParameter
 *
 * @brief   Set an TeraForce Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to right
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t TeraForce_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;

  switch ( param )
  {
    case FORCE_MEAS_START:
      if ( len == sizeof ( uint8 ) ) 
      {
        teraForceStartMeas = *((uint8*)value);
        if(teraForceStartMeas)
        {
        //  HalLedSet(HAL_LED_1 | HAL_LED_2, HAL_LED_MODE_ON); //Deixa led ligado Laranja durante medida.          
        }else
        {
           //HalLedSet(HAL_LED_ALL,HAL_LED_MODE_OFF); 
           //HalLedBlink(HAL_LED_1 , 0, 50, 1000); //Apos medida volta a piscar a cada 1 segundo o led verde
        }
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
      
    case FORCE_VAL_ATTR:
      if ( len == 16 ) 
      {      
        //teraForce_Force_val = *((int16*)value);
        VOID osal_memcpy( teraForce_Force_val, value, 16 );
        // See if Notification has been enabled
        GATTServApp_ProcessCharCfg( teraForce_ForceValConfig, (uint8 *)&teraForce_Force_val,
                                    FALSE, teraForceAttrTbl, GATT_NUM_ATTRS( teraForceAttrTbl ),
                                    INVALID_TASK_ID );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn      TeraForce_GetParameter
 *
 * @brief   Get a TeraForce Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t TeraForce_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
  
  case FORCE_MEAS_START:
    *((uint8*)value) = teraForceStartMeas;
    break;
    
  case FORCE_VAL_ATTR:
    //*((uint16*)value) = teraForce_Force_val;
    VOID osal_memcpy(  value, teraForce_Force_val,16 );
    break; 
  default:
    ret = INVALIDPARAMETER;
    break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn          teraForce_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       pAttr - pointer to attribute
 * @param       pLen - length of data to be read
 * @param       pValue - pointer to data to be read
 * @param       signature - whether to include Authentication Signature
 *
 * @return      Success or Failure
 */
static uint8 teraForce_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                               uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen )
{
  uint16 uuid;
  bStatus_t status = SUCCESS;

  // Make sure it's not a blob operation
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {    
    // 16-bit UUID
    uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those types for reads
    case FORCE_VAL_UUID:      
      *pLen = 16;
	  for (uint8 i=0;i<16;i++)
	  {
		pValue[i] =  *((uint8 *)pAttr->pValue+i);  
	  }
      break;
      
    case FORCE_START_MEAS_UUID:   
      *pLen = 1;
      pValue[0] = *pAttr->pValue;
      break;      
    default:
      // Should never get here!
      *pLen = 0;
      status = ATT_ERR_ATTR_NOT_FOUND;
      break;
    }
  }
  else
  {
    // 128-bit UUID
    *pLen = 0;
    status = ATT_ERR_INVALID_HANDLE;
  }


  return ( status );
}

/*********************************************************************
 * @fn      teraForce_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle � connection message was received on
 * @param   pReq - pointer to request
 *
 * @return  Success or Failure
 */
static bStatus_t teraForce_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                    uint8 *pValue, uint8 len, uint16 offset )
{
  bStatus_t status = SUCCESS;
  uint8 notify = 0xFF;

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      case FORCE_START_MEAS_UUID:
        //Validate the value
        // Make sure it's not a blob oper
        if ( offset == 0 )
        {
          if ( len > 1 )
            status = ATT_ERR_INVALID_VALUE_SIZE;
          else if ( pValue[0] != FALSE && pValue[0] != TRUE && pValue[0] != 2)//added the keep alive value
            status = ATT_ERR_INVALID_VALUE;
        }
        else
        {
          status = ATT_ERR_ATTR_NOT_LONG;
        }
        
        //Write the value
        if ( status == SUCCESS )
        {
          uint8 *pCurValue = (uint8 *)pAttr->pValue;
          
          *pCurValue = pValue[0];
          notify = FORCE_MEAS_START;        
        }
        break;  
   
          
      case GATT_CLIENT_CHAR_CFG_UUID:
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        break;      
          
      default:
          // Should never get here!
          status = ATT_ERR_ATTR_NOT_FOUND;
    }
  }
  else
  {
    // 128-bit UUID
    status = ATT_ERR_INVALID_HANDLE;
  }  

  switch (notify)
  {  
  case FORCE_MEAS_START:
  // If an attribute changed then callback function to notify application of change
  if ( teraForce_AppCBs && teraForce_AppCBs->pfnForceStartMeas )
    teraForce_AppCBs->pfnForceStartMeas();
    break;  
  default:
    break;
  }  
  return ( status );
}

/*********************************************************************
 * @fn          teraForce_HandleConnStatusCB
 *
 * @brief       Accelerometer Service link status change handler function.
 *
 * @param       connHandle - connection handle
 * @param       changeType - type of change
 *
 * @return      none
 */
static void teraForce_HandleConnStatusCB( uint16 connHandle, uint8 changeType )
{ 
  // Make sure this is not loopback connection
  if ( connHandle != LOOPBACK_CONNHANDLE )
  {
    // Reset Client Char Config if connection has dropped
    if ( ( changeType == LINKDB_STATUS_UPDATE_REMOVED )      ||
         ( ( changeType == LINKDB_STATUS_UPDATE_STATEFLAGS ) && 
           ( !linkDB_Up( connHandle ) ) ) )
    { 
      GATTServApp_InitCharCfg( connHandle, teraForce_ForceValConfig );      
     // GATTServApp_InitCharCfg( connHandle, accelZConfigCoordinates );
    }
  }
}


/*********************************************************************
*********************************************************************/
