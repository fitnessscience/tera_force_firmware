/**************************************************************************************************
  Filename:       accelerometer.h
  Revised:        $Date: 2013-09-05 15:48:17 -0700 (Thu, 05 Sep 2013) $
  Revision:       $Revision: 35223 $

  Description:    This file contains Accelerometer Profile header file.


  Copyright 2009 - 2013 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE, 
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com. 
**************************************************************************************************/

#ifndef SMARTBIA_H
#define SMARTBIA_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */


// Profile Parameters
#define FORCE_MEAS_START                0  // RW uint8 - Profile Attribute value
#define FORCE_VAL_ATTR                1  // N int16 - Profile Attribute value
//#define BIA_Q_VAL_ATTR                2  // N int16 - Profile Attribute value
//#define BIA_MEAS_POINT_ATTR           3  // RW unt8  - Profile Attribute value
//#define BIA_CABLE_CONNECTED_ATTR      4  // N  uint8 - Profile Attribute value
//#define BIA_FREQUENCY_ATTR            5  // RW  uint8 - Profile Attribute value
  
// Profile UUIDs
#define FORCE_START_MEAS_UUID           0xFCA1
#define FORCE_VAL_UUID                  0xFCA2
//#define BIA_Q_VAL_UUID                0xFDA3 
//#define BIA_MEAS_POINT_UUID           0xFDA4
//#define BIA_CABLE_CONNECTED_UUID      0xFDA5
//#define BIA_FREQUENCY_UUID            0xFDA6
  
// FORCE Service UUID
#define TERAFORCE_SERVICE_UUID            0xFCA0
  
// Profile Frequency Values
//CONST uint8 frequencyLUT[3]={1,50,100};//Frequency Ranges in KHz. 
  
// Accelerometer Profile Services bit fields
#define TERAFORCE_SERVICE                 0x00000001

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */
// Callback when the device has been started.  Callback event to 
// the ask for a battery check.
typedef void (*forceStartMeas_t)( void );

typedef struct
{
  forceStartMeas_t        pfnForceStartMeas;  // Called when Measure Start attribute changes  
} teraForceCBs_t;

/*********************************************************************
 * API FUNCTIONS 
 */

/*
 * Accel_AddService- Initializes the Accelerometer service by registering 
 *          GATT attributes with the GATT server. Only call this function once.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */
extern bStatus_t TeraForce_AddService( uint32 services );
       
/*
 * Accel_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t TeraForce_RegisterAppCBs( teraForceCBs_t *appCallbacks );


/*
 * Accel_SetParameter - Set an Accelerometer Profile parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t TeraForce_SetParameter( uint8 param, uint8 len, void *value );
 
/*
 * Accel_GetParameter - Get an Accelerometer Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t TeraForce_GetParameter( uint8 param, void *value );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* SMARTBIA_H */
