/**************************************************************************//**

    @file       AFFE4300.c

    @brief      Functions for accessing AFE4300 on CC2541 keyfob.

******************************************************************************/


/******************************************************************************
 * INCLUDES
 */
#include <ioCC2541.h>
#include "AFE4300.h"
#include "hal_mcu.h"
#include "hal_defs.h"

/******************************************************************************
 * DEFINES
 */
// Accelerometer connected at (names in brackets are BMA250 names)
// P2_0 = VDD (VDD/VDDIO)
// P1_2 = CS_N (CSB)
// P1_3 = SCK (SCx)
// P1_4 = MISO (SDO)
// P1_5 = MOSI (SDx)
// P1_7 = (INT1)

#define CS              P1_2
#define SCK             P1_3
#define MISO            P1_4
#define MOSI            P1_5
#define AFE_NRST        P0_5

#define AFE_NRST_ACTIVE       0
#define AFE_NRST_INACTIVE     1
#define CS_DISABLED     1
#define CS_ENABLED      0

#define AFE_READ_BIT   (0x01 << 5)
// OR bitmask into reg (read, modify, write)
#define ACC_INT_ENABLE(reg, bm)     { uint8 val; accReadReg((reg),&val); \
                                      val |= (bm); accWriteReg((reg),val); }
// AND inverted bitmask into reg (read, modify write)
#define ACC_INT_DISABLE(reg, bm)    { uint8 val; accReadReg((reg),&val); \
                                      val &= ~(bm); accWriteReg((reg),val); }


/******************************************************************************
 * FUNCTION PROTOTYPES
 */
void spiWriteByte(uint8 write);
void spiReadByte(uint8 *read, uint8 write);

/******************************************************************************
 * LOCAL VARIABLES
 */
#define SIZE_BUFFER_AMOSTRAS 8
static uint8 acc_initialized = FALSE;
int16 AFE_data[SIZE_BUFFER_AMOSTRAS];
uint8 num_conv=0;
uint8 conv_index =0; //Last recorded Conversion pos
/******************************************************************************
 * FUNCTIONS
 */

/**************************************************************************//**
* @fn       AFE_receiveData(void)
*
* @brief    Receive data from AFE4300
*
* @return   void
******************************************************************************/
void AFE_receiveData(int16* data)
{
  afeReadReg(0x00,data);  
} // AFE_receiveData



/**************************************************************************//**
* @fn       AFE_Init(void)
*
* @brief    Initialize SPI interface and AFE4300
*
* @return   void
******************************************************************************/
void AFE_Init(void)
{
  
  PERCFG |= (0x01 << 0);// USART 0 alt2
  P1SEL |= (0x01 << 3) | (0x01 << 4) | (0x01 << 5); //P1.3, P1.4, P1.5 as Peripheral Function
  P0SEL &= ~(0x01 <<5); // P0.5 as GPIO
  P0DIR |= (0x01 << 5);//P0.5 as output.NRST
  P1DIR |= (0x01 << 2);// P1.2 as Output. SSN out.
  P2DIR &= ~(0x01 << 0);  //P2.0 As input DRDY/
 
  CS = CS_DISABLED;
  U0CSR = 0x00; //SPI mAster Mode
  //      (MSB First) BAUD_E = 16 CPHA = 1
  //U0GCR =  (0x01 << 5) | (0x01 << 6) | 0x10; 
  //U0BAUD = 0x80;// SCK frequency = 3MHz (AFE4300 max=4MHz, CC254x max = 4MHz)//BAUD_M
  U0GCR =  (0x01 << 5) | (0x01 << 6) | 0x11;
  U0BAUD = 0x00;// SCK frequency = 4MHz (AFE4300 max=4MHz, CC254x max = 4MHz)//BAUD_M

  //WAIT_MS(2); //TODO verificar se precisa.
  
   //Enable P2.0 Interrupt 
  P2IFG = ~(0x01 << 0);//Clear Flag P2.0
  
  PICTL |= (0x01 << 3); //P2.0-4 Falling Edge
  P2IEN |= (0x01 << 0); //P2.0 Int enabled
  IEN2 |= (0x01 << 1); //Port 2 int enabled
  //IEN0 |= 0x01 << 7; //Global int enabled.
  
  AFE_NRST = AFE_NRST_INACTIVE;
  //WAIT_MS(1);
  asm("NOP");
  AFE_NRST = AFE_NRST_ACTIVE;
  asm("NOP");
  //WAIT_MS(1);
  AFE_NRST = AFE_NRST_INACTIVE;
  asm("NOP");
  //WAIT_MS(1);
  afeWriteReg(DEVICE_CONTROL1,0x6004);  
  afeWriteReg(MISC_REGISTER1,0x0000);
  afeWriteReg(MISC_REGISTER2,0xFFFF);
  afeWriteReg(MISC_REGISTER3,0x0030);
  afeWriteReg(ADC_CONTROL_REGISTER1,0x61C0);
  afeWriteReg(BCM_DAC_FREQ,0x0040);
  afeWriteReg(DEVICE_CONTROL2,0x0000);
  afeWriteReg(ADC_CONTROL_REGISTER2,0x0011);
  num_conv =0;
  conv_index =0;  
} // AFE_Init

/**************************************************************************************************
 * @fn      halKeyPort2Isr
 *
 * @brief   Port2 ISR
 *
 * @param
 *
 * @return
 **************************************************************************************************/

HAL_ISR_FUNCTION( halKeyPort2Isr, P2INT_VECTOR )
{
  HAL_ENTER_ISR();
  
  //extern int8 received_rssi;
  if (P2IFG & (1<<0))
  {
   conv_index = (num_conv++)&(SIZE_BUFFER_AMOSTRAS-1);  
   AFE_receiveData(&AFE_data[conv_index]);  
   //AFE_data[conv_index]= (int16)received_rssi;
  }

  /*
    Clear the CPU interrupt flag for Port_2
    PxIFG has to be cleared before PxIF
    Notes: P2_1 and P2_2 are debug lines.
  */
  P2IFG = 0;
  P2IF = 0;

  CLEAR_SLEEP_MODE();

  HAL_EXIT_ISR();

  return;
}
uint8 read_ptr = 0; 
void AFE4300_read_last_conversions(int16* buffer, uint8 num_amostras)
{  
  uint8 i;
  HAL_DISABLE_INTERRUPTS(); 
  for (i=0; i< num_amostras;i++)
  {
    buffer[i] = AFE_data[i];
  }
  num_conv=0;
  HAL_ENABLE_INTERRUPTS();
}
int16 AFE4300_read_media(uint8 num_amostras)
{
  int32 soma=0;
  uint8 i;
  HAL_DISABLE_INTERRUPTS(); 
  for (i=0; i< num_amostras;i++)
  {
    soma += AFE_data[(num_conv-i-1) & (SIZE_BUFFER_AMOSTRAS-1)];
  }
    HAL_ENABLE_INTERRUPTS();
  return(soma/num_amostras);  
}
/**************************************************************************//**
* @fn       accStop(void)
*
* @brief    Sets the BMA250 accelerometer in low-power mode.
*
* @return   void
******************************************************************************/
/*
void accStop(void)
{
  if (acc_initialized) {
    // We cheat and simply turn off power to the accelerometer
    P2_0  =  0x00;  // If init has been run this pin is already output.
    acc_initialized = FALSE;
  }
}
*/
/**************************************************************************//**
* @fn       accWriteReg(uint8 reg, uint8 val)
*
* @brief    Write one byte to a sensor register
*
* @param    reg     Register address
* @param    val     Value to write
*
* @return   void
******************************************************************************/
void afeWriteReg(uint8 reg, uint16 val)
{
    CS = CS_ENABLED;
    spiWriteByte(reg);      // Write address
    spiWriteByte((uint8)(val >> 8));      // Write value
    spiWriteByte((uint8)(val & 0x00FF));      // Write value
    CS = CS_DISABLED;
}


/**************************************************************************//**
* @fn       accReadReg(uint8 reg, uint8 *pVal)
*
* @brief    Read one byte from a sensor register
*
* @param    reg     Register address
* @param    val     Pointer to destination of read value
*
* @return   void
******************************************************************************/
void afeReadReg(uint8 reg, uint16 *pVal)
{
    uint8 highbyte,lowbyte;  
    CS = CS_ENABLED;
    spiWriteByte(AFE_READ_BIT|reg);     // Write address | READ bit
    spiReadByte(&highbyte, 0xFF);    // Write dummy data and read returned value
    spiReadByte(&lowbyte, 0xFF);    // Write dummy data and read returned value
    CS = CS_DISABLED;
    *pVal = BUILD_UINT16(lowbyte,highbyte);
}


/**************************************************************************//**
* @fn       accReadAcc(int16 *pXVal, int16 *pYVal, int16 *pZVal)
*
* @brief    Read x, y and z acceleration data in one operation.
*
* @param    pXVal   Pointer to destination of read out X acceleration
* @param    pYVal   Pointer to destination of read out Y acceleration
* @param    pZVal   Pointer to destination of read out Z acceleration
*
* @return   void
******************************************************************************/
/*
void accReadAcc(int8 *pXVal, int8 *pYVal, int8 *pZVal)
{
    int8 readout[6] = {0,0,0,0,0,0};

    // Read all data from accelerometer
    CS = CS_ENABLED;
    spiWriteByte(0x80);     // Write start address
    for(uint8 i = 0; i<6; i++)
    {
        spiReadByte((uint8 *)&readout[i], 0xFF); // Read byte
    }
    CS = CS_DISABLED;

    // Use only most significant byte of each channel.
    *pXVal = readout[1];
    *pYVal = readout[3];
    *pZVal = readout[5];

} // accReadAcc
*/

/**************************************************************************//**
* @fn       accReadAcc(int16 *pXVal, int16 *pYVal, int16 *pZVal)
*
* @brief    Read x, y and z acceleration data in one operation.
*
* @param    pXVal   Pointer to destination of read out X acceleration
* @param    pYVal   Pointer to destination of read out Y acceleration
* @param    pZVal   Pointer to destination of read out Z acceleration
*
* @return   void
******************************************************************************/
/*
void accReadAcc16(int16 *pXVal, int16 *pYVal, int16 *pZVal)
{
    int8 readout[6] = {0,0,0,0,0,0};

    // Read all data from accelerometer
    CS = CS_ENABLED;
    spiWriteByte(0x80);     // Write start address
    for(uint8 i = 0; i<6; i++)
    {
        spiReadByte((uint8 *)&readout[i], 0xFF); // Read byte
    }
    CS = CS_DISABLED;

    // Merge high byte (8b) and low bits (2b) into 16b signed destination
    *pXVal = ( (((uint8)readout[0]) >> 6) | ((int16)(readout[1]) << 2) );
    *pYVal = ( (((uint8)readout[2]) >> 6) | ((int16)(readout[3]) << 2) );
    *pZVal = ( (((uint8)readout[4]) >> 6) | ((int16)(readout[5]) << 2) );

} // accReadAcc16
*/
 
/**************************************************************************//**
* @fn       spiWriteByte(uint8 write)
*
* @brief    Write one byte to SPI interface
*
* @param    write   Value to write
******************************************************************************/
void spiWriteByte(uint8 write)
{
        U0CSR &= ~0x02;                 // Clear TX_BYTE
        U0DBUF = write;
        while (!(U0CSR & 0x02));        // Wait for TX_BYTE to be set
}


/**************************************************************************//**
* @fn       spiReadByte(uint8 *read, uint8 write)
*
* @brief    Read one byte from SPI interface
*
* @param    read    Read out value
* @param    write   Value to write
******************************************************************************/
void spiReadByte(uint8 *read, uint8 write)
{
        U0CSR &= ~0x02;                 // Clear TX_BYTE
        U0DBUF = write;                 // Write address to AFE
        while (!(U0CSR & 0x02));        // Wait for TX_BYTE to be set
        *read = U0DBUF;                 // Save returned value
}

void init_pwm(int32 freq)
{
 // uint8 reg_clkconsta;
  uint16 tmp;
  uint32 tmp2;
  if((freq <= 32000000 / 0xFFFF) || freq == 0)
  {
   return; 
  }
  //tmp = (uint16)(32000000 / freq) -1;
  //tmp2 = (uint32)(32000000%freq);
  //if(tmp2  > (freq >> 1)) //rounding upwards if fractional part > 0.5
  //{
  //  tmp++;
  //}
  tmp=32;
  PERCFG &= ~(0x01 << 6);  //T1 ALT loc 1
  APCFG &= ~(0x01 << 6);  //P0.6 ANALOG Disabled
  P0SEL |= 0x01 << 6;     //P0.6 As Peripheral function
  P0DIR |= 0x01 << 6;     //P0.6 as output.  
  CLKCONCMD &=  ~(0x07 << 3); //TICKSPD = 32MHz.
  T1CTL = 0x00; //timer Supended; Tick /1
  T1CCTL4 = (0x03 << 3) | (0x01 << 2); //Compare mode 3
  T1CC0L = (uint8)tmp & 0xFF; //Set period to 40 ticks of 32Mhz.
  T1CC0H = (uint8)((tmp >> 8) & 0xFF);
  tmp = tmp >> 1; //divide the period by 2 to get 50% duty
  T1CC4L = (uint8)tmp & 0xFF; //Set duty cycle close to 50% (20 ticks)
  T1CC4H = (uint8)((tmp >> 8) & 0xFF);
  T1CNTL = 0x00;
  T1CTL = 0x02; //timer in Modulo mode; Tick /1
  //reg_clkconsta = CLKCONSTA;  
}

void stop_pwm(void)
{
  T1CTL = 0x00; //Timer Suspended; Tick /1
}

/******************************************************************************
  Copyright 2011 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED ?AS IS? WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
******************************************************************************/
