
#include "atecc108.h"
#include "hal_i2c.h"
#include "hal_crc.h"




void ATEEC108_init()
{
  HalI2CInit(ATECC108_SLAVE_ADDRESS,i2cClock_123KHZ);
  HalCRCInit(0x0000);
}
void ATECC108_WakeUp(void)
{
  I2CWC = (0x01 << 7) | (0x01 <<0);
  I2CIO = (0x01 << 0);
  I2CIO &= ~(0x01 << 0);
  WAIT_MS(1);
  I2CIO |= (0x01 << 0);
  I2CWC &= ~(0x01 << 7);  
}

uint8 reflect8bits(uint8 crc)
{
  // reflects the 8 bits of 'crc'
  
  uint8 i, j=1, crcout=0;
  for (i=(uint8)1<<7; i; i>>=1) 
  {
    if (crc & i) crcout|=j;
	j<<= 1;
  }
  return (crcout);
}

void ATECC108_Calc_CRC(atecc108_packet_t *packet)
{
  HalCRCInit(0x0000);
  HalCRCExec(reflect8bits(packet->length));
  HalCRCExec(reflect8bits(packet->command));
  if (packet->payload != NULL)
  {
    for(uint8 i=0; i < packet->length-2; i++)
    {
      HalCRCExec(reflect8bits((packet->payload)[i]));
    }
  }
  packet->CRC = HalCRCCalc();
}
