/**************************************************************************************************
  Filename:       HostTest_Main.c
  Revised:        $Date: 2011-05-05 16:06:18 -0700 (Thu, 05 May 2011) $
  Revision:       $Revision: 25882 $

  Description:    This file contains the main and callback functions


  Copyright 2009 - 2011 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/**************************************************************************************************
 *                                           Includes
 **************************************************************************************************/
/* Hal Drivers */
#include "hal_types.h"
#include "hal_key.h"
#include "hal_timer.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "hal_crc.h"
#include "hal_i2c.h"
#include "hal_adc.h"

/* OSAL */
#include "OSAL.h"
#include "OSAL_Tasks.h"
#include "OSAL_PwrMgr.h"
#include "osal_snv.h"
#include "OnBoard.h"
/*DEBUG*/
#include "AFE4300.h"
#include "atecc108.h"
/**************************************************************************************************
 * FUNCTIONS
 **************************************************************************************************/

/* This callback is triggered when a key is pressed */
void MSA_Main_KeyCallback(uint8 keys, uint8 state);

/**************************************************************************************************
 * @fn          main
 *
 * @brief       Start of application.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
int main(void)
{  
  
  /* Initialize hardware */
   HAL_BOARD_INIT();

  // Initialize board I/O
  InitBoard( OB_COLD );
  /*
  P1DIR |= 1 <<1;
  while(1)
  {
    P1_1 ^= 1;  
    WAIT_MS(1000);
  }
  */
  //IQ_DEMOD_CLK = fCLK / (IQ_DEMOD_CLK_DIV_FAC) = BCM_DAC_FREQ � 4)
  //dac_freq = 50000; //desired DAC FREQ.
 // iq_demod_freq = 50000 << 2;
  //The DAC output frequency is given by DAC[9:0] � fCLK / 1024   -> 50KHz = bcm_dac_freq * fCLK /1024
 //bcm_dac_freq = 64; //800000 * 64 /1024
 //IQ_DEMOD_CLK_DIV_FAC = 800000 / iq_demod_freq =4
  
 //  AFE_Init();  
 
/*
  AFE_Init();  
  init_pwm(800000);
  afeWriteReg(VSENSE_MUX,R_680);//VSENSP_R1 e VSENSEM_R0
  afeWriteReg(ISW_MUX,R_680);
  afeWriteReg(DEVICE_CONTROL1,(1<<1)| (1<<2)| (0x03 << 13));
  //afeWriteReg(ADC_CONTROL_REGISTER2, (0x03 << 5) | (0x3 << 0));// ADC conected to BCM I DEMOD /FWR , Internal VREF
  afeWriteReg(ADC_CONTROL_REGISTER2, (0x03 << 5) | (0x5 << 0));// ADC conected to BCM Q DEMOD  , Internal VREF
  afeWriteReg(DEVICE_CONTROL2, 0x0000); //Reset
  afeWriteReg(BCM_DAC_FREQ,0x0000); //Reset
  afeWriteReg(BCM_DAC_FREQ,0x0040); //DAC freq (64)*800KHz /1024 = 50kHz 
  afeWriteReg(DEVICE_CONTROL2, (0x02 << 11)); //IQ_DEMOD_CLK_DIV_FAC = 2 (divide by 4)
  afeWriteReg(IQ_MODE_ENABLE, (0x01 << 11)); //IQ_MODE enabled
  afeWriteReg(ADC_CONTROL_REGISTER1,0x4140); //128SPS continuous conv DIFFERENTIAL
  WAIT_MS(990);
  //afeWriteReg(ADC_CONTROL_REGISTER1,0xC1C0); //PowerDown ADC
  while(1);  
*/
   /* Initialze the HAL driver */
  HalDriverInit();
  /*
  uint8 command_buffer[160];
  uint8 dummy=0x00;
  uint8 val;
  uint16 CRC_val;
  ATECC108_WakeUp();
  WAIT_MS(2); //minimum 1.5 ms to ATECC leave sleep state and be ready to receive commands. 
  HalI2CInit(0x60,i2cClock_123KHZ);
  HalCRCInit(0x0000);
  command_buffer[0] = 0x03; //Word address
  command_buffer[1] = 0x07; //COUNT
  command_buffer[2] = 0x02; //OP Code (read)
  command_buffer[3] = 0x80; //Param1 (read 32 bytes)
  command_buffer[4] = 0x00; //Param2[0]
  command_buffer[5] = 0x00; //Param2[1]
  for(uint8 i = 1; i < (command_buffer[1] - 1); i++)
  {
    HalCRCExec(reflect8bits(command_buffer[i]));
  }  
  CRC_val = HalCRCCalc();
  command_buffer[6] =  LO_UINT16(CRC_val);
  command_buffer[7] =  HI_UINT16(CRC_val);
  val = HalI2CWrite(command_buffer[1]+1,command_buffer);     
  WAIT_MS(1);
  val = HalI2CRead(32,command_buffer);
  */
/*  
  APCFG =  (0x01 << 0); //P0.0 as analog input
  uint16 adc_value;
  uint8 CableConnected;
  HalAdcSetReference(HAL_ADC_REF_AVDD);
  //adc_value =  HalAdcRead(HAL_ADC_CHANNEL_0, HAL_ADC_RESOLUTION_14);
  while(1)
  {
    adc_value =  HalAdcRead(HAL_ADC_CHANNEL_0, HAL_ADC_RESOLUTION_14);
    //CABLE CONNECTED : 4793
    //CABLE CONNECTED + Button Pressed: 1007 
    // Button Pressed: 1068
    //NO cable NO button pressed: 7962
    if((adc_value < 5000) && (adc_value > 1500) || (adc_value < 1050 ))
    {
      CableConnected = 1;
       HalLedSet(HAL_LED_GREEN, HAL_LED_MODE_ON);
       HalLedSet(HAL_LED_RED, HAL_LED_MODE_OFF);
    }else
    {
       CableConnected = 0;
       HalLedSet(HAL_LED_RED, HAL_LED_MODE_ON);
       HalLedSet(HAL_LED_GREEN, HAL_LED_MODE_OFF);
    }
   
  }
  */
  
  
  /* Initialize NV system */
  osal_snv_init();
  
  /* Initialize LL */

  /* Initialize the operating system */
  osal_init_system();

  /* Enable interrupts */
  HAL_ENABLE_INTERRUPTS();

  // Final board initialization
  InitBoard( OB_READY );

  #if defined ( POWER_SAVING )
    //osal_pwrmgr_device( PWRMGR_BATTERY );
  #endif  
    
  /* Start OSAL */
  osal_start_system(); // No Return from here

  return 0;
}

/**************************************************************************************************
                                           CALL-BACKS
**************************************************************************************************/


/*************************************************************************************************
**************************************************************************************************/
