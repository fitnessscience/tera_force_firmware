#include <ioCC2541.h>
#include "AFE4300.h"
#include "hal_mcu.h"
#include "hal_defs.h"

//Defines
#define ATECC108_SLAVE_ADDRESS 0x60

//Word Address
#define WORD_ADDRESS_RESET      0x00
#define WORD_ADDRESS_SLEEP      0x01
#define WORD_ADDRESS_IDLE       0x02
#define WORD_ADDRESS_COMMAND    0x03

//STATUS/ERROR Codes

#define STATUS_SUCCESS                          0x00
#define ERROR_CHECKMAC_VERIFY_MISCOMPARE        0x01
#define ERROR_PARSE_ERROR                       0x03
#define ERROR_ECC_FAULT                         0x05
#define ERROR_EXECUTION                         0x0F
#define STATUS_AWAKE                            0x11
#define ERROR_CRC_OTHER                         0xFF

//Commands

#define COMMAND_CHECKMAC	0x28
#define COMMAND_DERIVEKEY	0x1C
#define COMMAND_GENDIG	        0x15
#define COMMAND_GENKEY	        0x40
#define COMMAND_HMAC	        0x11
#define COMMAND_INFO	        0x30
#define COMMAND_LOCK	        0x17
#define COMMAND_MAC	        0x08
#define COMMAND_NONCE	        0x16
#define COMMAND_PAUSE	        0x01
#define COMMAND_PRIVWRITE	0x46
#define COMMAND_RANDOM	        0x1B
#define COMMAND_READ	        0x02
#define COMMAND_SIGN	        0x41
#define COMMAND_TEMPSENSE	0x18
#define COMMAND_UPDATEEXTRA	0x20
#define COMMAND_VERIFY	        0x45
#define COMMAND_WRITE	        0x12


//Zone ENCODING

#define CONFIG  0x00
#define OPT     0x01
//#define DATA    0x02








//Types
typedef struct atecc108_packet 
{
  uint8 word_address;
  uint8 length;
  uint8 command;
  uint8 *payload;
  uint16 CRC;  
}atecc108_packet_t;

//Exported Functions
void ATEEC108_init();
void ATECC108_WakeUp(void);
void ATECC108_Calc_CRC(atecc108_packet_t *packet);




//Internal Functions
uint8 reflect8bits(uint8 crc);